 /**
   ******************************************************************************
   * @file mlx90614.c
   * @author  Caisson Elektronik
   * @version V1.0
   * @date 14-05-2018
   * @brief   Implementation of functions for sensor access.
   *
   ***************************************************************************************
   * @attention
   * Library can be used in every STM32 application based on HAL (C language)
   ***************************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "mlx90614.h" /**< Include here HAL header for your MCU */
#include "i2c.h"
/* Variables -----------------------------------------------------------------*/
uint8_t mlx90614_buffor[3];
volatile uint8_t mlx90614_SMBus_TxCplt_flag = 0; /**< Transmit complete flag*/ 
volatile uint8_t mlx90614_SMBus_RxCplt_flag = 0; /**< Receive complete flag */
uint8_t mlx90614_command; 
mlx90614_Status_t Error_status;

/**
	* @addtogroup MLX90614_LIB MLX90614 library
	* @{
*/

/**
	* @defgroup MLX90614_FUNCTIONS Functions
	* @brief Functions collection
	* @{
*/

/**
	* @brief Get temperature from sensor. Before use check RAM command in header file to choose
	* desired temperature measurement.
	* @param [in] SMBUS_HandleTypeDef* SMBUSx -> Pointer to a SMBUS_HandleTypeDef structure.
	* @param [in] uint8_t TempType -> RAM command Macro.
	* @retval float -> Measurements temperature in Celsius degrees.
*/
float mlx90614_Get_Temp(SMBUS_HandleTypeDef *SMBUSx, uint8_t TempType)
{
	float result;
	uint16_t rawTemp;
	mlx90614_command = TempType;
	mlx90614_SMBus_TxCplt_flag = 0;
	uint8_t buffor[3];
	
	/* Read temperature from sensor without PEC. */
	Error_status = mlx90614_Read_Data(SMBUSx, mlx90614_command, buffor, 0);
	
	/* Get results from array and calculate temperature in Celsius degrees. */
	rawTemp = ((mlx90614_buffor[1] << 8) | mlx90614_buffor[0]);
	result = rawTemp * 0.02 - 273.15;
	
	/* Clear Rx complete flag. */
	mlx90614_SMBus_RxCplt_flag = 0;
	
	/* Check the range and return data. */
	if(result < -50.1 || result > 250.1) return -1;
		else return result;
}

/**
	* @brief This function handles SMBus Transmit Transfer Complete interrupt.
*/
void HAL_SMBUS_MasterTxCpltCallback(SMBUS_HandleTypeDef *hsmbus)
{
	mlx90614_SMBus_TxCplt_flag = 1;
}

/**
	* @brief This function handles SMBus Receive Transfer Complete interrupt.
*/
void HAL_SMBUS_MasterRxCpltCallback(SMBUS_HandleTypeDef *hsmbus)
{
	mlx90614_SMBus_RxCplt_flag = 1;
}

/**
	* @brief Function reads data from sensor depending on the command - RAM or EEPROM.
	* @param [in] SMBUS_HandleTypeDef* SMBUSx -> Pointer to a SMBUS_HandleTypeDef structure.
	* @param [in] uint8_t command -> Command macro.
	* @param [in] uint8_t *bufferRead -> table to save received data.
	* @retval mlx90614_Status_t -> Return code of error.
	* @see mlx90614_Status_t
*/
mlx90614_Status_t mlx90614_Read_Data(SMBUS_HandleTypeDef *SMBUSx, uint8_t command, uint8_t *bufferRead, uint8_t PEC)
{
	mlx90614_Status_t status;
	mlx90614_command = command;
	mlx90614_SMBus_TxCplt_flag = 0;
	
	/* Data to calculate PEC format: */
	/* MLX90614 Address write, Command, MLX90614 Address read, LSB receive data, MSB receive data */
	uint8_t PECdata[5] = {MLX90614_ADDR, command, MLX90614_ADDR+1, 0, 0};
	
	/* Send command to MLX90614 */
	if(HAL_SMBUS_Master_Transmit_IT(SMBUSx, (uint16_t)MLX90614_ADDR, &mlx90614_command, 1, SMBUS_FIRST_FRAME) != HAL_OK) status = SMBUS_ERROR;
		else status = NO_ERROR;
	
	/* Wait to Tx complete and receive data from MLX9014 */
	while(mlx90614_SMBus_TxCplt_flag != 1);
	if(HAL_SMBUS_Master_Receive_IT(SMBUSx, (uint16_t)MLX90614_ADDR, mlx90614_buffor, 3, SMBUS_FIRST_AND_LAST_FRAME_WITH_PEC) != HAL_OK) status = SMBUS_ERROR;
		else status = NO_ERROR;
	
	/* Wait to Rx complete and save data in output buffer. Bit order MSB, LSB */
	while(mlx90614_SMBus_RxCplt_flag != 1);
	bufferRead[0] = mlx90614_buffor[1]; 
	bufferRead[1] = mlx90614_buffor[0];
	
	/* Check PEC */
	if(PEC == 1)
	{

		PECdata[3] = mlx90614_buffor[0];
		PECdata[4] = mlx90614_buffor[1];
		if(mlx90614_Check_PEC(PECdata, 5, mlx90614_buffor[2]) != NO_ERROR) status = PEC_ERROR;
	}
	
	/* Reset Rx flag */
	mlx90614_SMBus_RxCplt_flag = 0;
	
	status = NO_ERROR;
	return status;
}

/**
	* @brief Function write data to sensor depending on EEPROM the command .
	* @param [in] SMBUS_HandleTypeDef* SMBUSx -> Pointer to a SMBUS_HandleTypeDef structure.
	* @param [in] uint8_t command -> Write command macro.
	* @param [in] uint16_t value_to_set-> Variable with value to set.
	* @retval mlx90614_Status_t -> Return code of error.
	* @see mlx90614_Status_t
*/
mlx90614_Status_t mlx90614_Write_Data(SMBUS_HandleTypeDef *SMBUSx, uint8_t command, uint16_t value_to_set)
{
	mlx90614_SMBus_TxCplt_flag = 0;
	uint8_t writeBuf[4] = {command, value_to_set, (value_to_set >> 8), 0};	
	
	/* Data to calculate PEC format: */
	/* MLX90614 Address write, Command, LSB receive data, MSB receive data */
	uint8_t PECdata[4] = {MLX90614_ADDR, writeBuf[0], writeBuf[1], writeBuf[2]};
	
	/* Calculate PEC */
	writeBuf[3] = mlx90614_Calc_PEC(PECdata,4);
	
	/* Send command to MLX90614 */
	if(HAL_SMBUS_Master_Transmit_IT(SMBUSx, (uint16_t)MLX90614_ADDR, writeBuf, 4, SMBUS_LAST_FRAME_WITH_PEC) != HAL_OK) return SMBUS_ERROR;
	
	/* Wait to Tx complete. */
	while(mlx90614_SMBus_TxCplt_flag != 1);
	
	return NO_ERROR;
}

/**
	* @brief Function calculates PEC based on polynomial from datasheet p. 14 s. 7.4.3.1 Bus Protocol
	* @param [in] uint8_t* data -> Handle to data. 
	* @param [in] uint8_t nbrOfBytes -> Number of bytes to calculate PEC.
	* @retval uint8_t PEC - calculated PEC
*/
uint8_t mlx90614_Calc_PEC(uint8_t *data, uint8_t nbrOfBytes)
{
	uint8_t bit;
	uint8_t crc = 0x00;
	uint8_t byteCtr;
	
	// calculates 8-Bit checksum with given polynomial 
	for(byteCtr = 0; byteCtr < nbrOfBytes; byteCtr++) 
	{ 
		crc ^= (data[byteCtr]); 
		for(bit = 8; bit > 0; --bit) 
		{ 
			if(crc & 0x80) crc = (crc << 1) ^ MLX90614_POLYNOMIAL; 
			else crc = (crc << 1); 
		} 
	}
 return crc;
}

/**
	* @brief Function compare calculating PEC of input data and PEC gets from SHT3x.
	* @param [in] uint8_t* data -> Handle to data. 
	* @param [in] uint8_t nbrOfBytes -> Number of bytes to calculate PEC.
	* @param [in] uint8_t checksum -> Checksum from SHT3X.
	* @retval uint8_t PEC
*/
mlx90614_Status_t mlx90614_Check_PEC(uint8_t* data, uint8_t nbrOfBytes, uint8_t checksum)
{
	uint8_t crc; 
	crc = mlx90614_Calc_PEC(data, nbrOfBytes);
	if(crc != checksum) return PEC_ERROR;
	else return NO_ERROR;
}

/**
	* @}
*/

/**
	* @}
*/

 /**
   ******************************************************************************
   * @file mlx90614.h
   * @author  Caisson Elektronik
   * @version V1.0
   * @date 14-05-2018
   * @brief   Definitions of commands and functions for sensor access.
   *
   ***************************************************************************************
   * @attention
   * Library can be used in every STM32 application based on HAL (C language)
   ***************************************************************************************
*/

#ifndef __MLX90614_H
#define __MLX90614_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h" /**< Include here HAL header for your MCU */

/** @defgroup MLX90614_LIB MLX90614 library
  * @brief Collection of data structures and functions
  * @{
  */
	
/**
	* @defgroup MLX_MACROS Macros
	* @brief Macros collection
	* @{
*/

/* Polynomial for PEC --------------------------------------------------------*/
#define MLX90614_POLYNOMIAL  0x107 // P(x) = x^8 + x^2 + x^1 + 1 = 100000111

/* EEPROM Register -----------------------------------------------------------*/
#define MLX90614_EEPROM_MASK 					0x20

#define MLX90614_TOMAX								MLX90614_EEPROM_MASK + 0x00 /**< Max object temperature range value in PWM mode. See datasheet p.24*/
#define MLX90614_TOMIN								MLX90614_EEPROM_MASK + 0x01 /**< Min object temperature range value in PWM mode. See datasheet p.24*/
#define MLX90614_PWMCTRL							MLX90614_EEPROM_MASK + 0x02 /**< PWM control register. */
#define MLX90614_TA_RANGE							MLX90614_EEPROM_MASK + 0x03 /**< Ambient temperature range. The 8 MSB are foreseen for max ambient temperature and the 8 LSB for min ambient temperature. */
#define MLX90614_EMIS_CORRECT_COEFF		MLX90614_EEPROM_MASK + 0x04 /**< Register to change emissivity in the range 0.1 .. 1.0. Default MLX90614 is calibrated for an object emissivity 1.0 (0xFFFF). See datasheet p.11*/
#define MLX90614_CONFIG_REG						MLX90614_EEPROM_MASK + 0x05 /**< Configuration register. See datasheet p.12 Table 7. */
#define MLX90614_SMBUS_ADDR						MLX90614_EEPROM_MASK + 0x0E /**< SMBus Address in 8 LSB. */

/* PWM control register settings ---------------------------------------------*/

/* PWM mode ------------------------------------------------------------------*/
#define MLX90614_PWM_SINGLE_MODE 		(0 << 0)
#define MLX90614_PWM_EXTENDED_MODE 	(1 << 0)

/* PWM Power -----------------------------------------------------------------*/
#define MLX90614_PWM_DISABLE 				(0 << 1)
#define MLX90614_PWM_ENABLE 				(1 << 1)

/* SDA Pin configuration -----------------------------------------------------*/
#define MLX90614_PWM_SDA_OD 				(0 << 2) /**< SDA pin configured as Open Drain. */
#define MLX90614_PWM_SDA_PP					(1 << 2) /**< SDA pin configured as Push-Pull. */

/* PWM/Thermal relay mode select ---------------------------------------------*/
#define MLX90614_PWM_MODE				(0 << 3) /**< PWM mode selected . */
#define MLX90614_TH_RELAY				(1 << 3) /**< Thermal relay mode selected . */

/* Configuration register settings -------------------------------------------*/

/* IIR settings --------------------------------------------------------------*/
#define MLX90614_IIR_100	(1 << 2)
#define MLX90614_IIR_80		(1 << 2 | 1 << 0)
#define MLX90614_IIR_67		(1 << 2 | 1 << 1)
#define MLX90614_IIR_57		(1 << 2 | 1 << 1 | 1 << 0)
#define MLX90614_IIR_50		(0 << 0)
#define MLX90614_IIR_25		(1 << 0)
#define MLX90614_IIR_17		(1 << 1)
#define MLX90614_IIR_13		(1 << 1 | 1 << 0)

/* Repeat sensor test --------------------------------------------------------*/
#define MLX90614_REPEAT_SENSOR_OFF 	(0 << 3)
#define MLX90614_REPEAT_SENSOR_ON 	(1 << 3)

/* Measurement enable --------------------------------------------------------*/
#define MLX90614_TA_TOBJ1_EN 				(0 << 4)
#define MLX90614_TA_TOBJ2_EN			 	(1 << 4) /**< Enable ambient temperature and object 2 temperature. */
#define MLX90614_TOBJ2_EN 					(1 << 5) /**< Enable object 2 temperature. */ 
#define MLX90614_TOBJ1_TOBJ2_EN 		(1 << 5 | 1 << 4) /**< Enable object 2 temperature. */ 

/* Sensor IR quantity --------------------------------------------------------*/
#define MLX90614_SINGLE_IR				(0 << 6) 
#define MLX90614_DUAL_IR					(1 << 6) 

/* Sign of Ks ----------------------------------------------------------------*/
#define MLS90614_POS_KS_SIGN		(1 << 7) /**< Positive sign of Ks. */
#define MLS90614_NEG_KS_SIGN		(1 << 7) /**< Negative sign of Ks. */

/* FIR settings --------------------------------------------------------------*/
#define MLX90614_FIR_8			(0 << 8) /**< NOT RECOMMENDED */
#define MLX90614_FIR_16			(1 << 8) /**< NOT RECOMMENDED */
#define MLX90614_FIR_32			(1 << 9) /**< NOT RECOMMENDED */
#define MLX90614_FIR_64			(1 << 9 | 1 << 8) /**< NOT RECOMMENDED */
#define MLX90614_FIR_128		(1 << 10)
#define MLX90614_FIR_256		(1 << 10 | 1 << 8)
#define MLX90614_FIR_512		(1 << 10 | 1 << 9)
#define MLX90614_FIR_1024		(1 << 10 | 1 << 9 | 1 << 8)

/* Gain settings --------------------------------------------------------------*/
#define MLX90614_GAIN_1				(0 << 11)
#define MLX90614_GAIN_3				(1 << 11)
#define MLX90614_GAIN_6				(1 << 12)
#define MLX90614_GAIN_12_5		(1 << 12 | 1 << 11)
#define MLX90614_GAIN_25			(1 << 13)
#define MLX90614_GAIN_50			(1 << 13 | 1 << 11)
#define MLX90614_GAIN_100			(1 << 13 | 1 << 12)
#define MLX90614_GAIN_200			(1 << 13 | 1 << 12 | 1 << 11)

/* Sign of Kt2 ----------------------------------------------------------------*/
#define MLX90614_POS_KT2_SIGN		(0 << 14) /**< Positive sign of Ks. */
#define MLX90614_NEG_KT2_SIGN		(1 << 14) /**< Negative sign of Ks. */

/* Enable/Disable sensor test -------------------------------------------------*/
#define MLX90614_TEST_EN			(0 << 15) /**< Enable sensor test. */
#define MLX90614_TEST_DIS			(1 << 15) /**< Disable sensor test. */

/* RAM Command ---------------------------------------------------------------*/
#define MLX90614_TA 		0x06 /**< Ambient temperature result. */
#define MLX90614_TOBJ1	0x07 /**< Temperature object 1 result. */
#define MLX90614_TOBJ2	0x08 /**< Temperature object 2 result. */

/* SMBus Address --------------------------------------------------------------*/
#define MLX90614_ADDR		0xB4 /**< Default address */

/**
	* @}
*/

/**
	* @defgroup MLX90614_VARIABLES Variables
	* @brief Variables collection
	* @{
*/

extern volatile uint8_t mlx90614_SMBus_TxCplt_flag; /**< Transmit complete flag*/ 
extern volatile uint8_t mlx90614_SMBus_RxCplt_flag; /**< Receive complete flag */

/**
	* @}
*/

/**
	* @defgroup MLX90614_ENUMERATION Enums
	* @brief Enums collection
	* @{
*/

/**                                                                      
	* @enum mlx90614_Status_t
	* @brief Every function can return five type of error - useful in debugging.
	* @var NO_ERROR
	* No error.
	* @var SMBUS_ERROR
	* SMBUS error. For more information check HAL Status in HAL.
	* @var PEC_ERROR
	* Checksum mismatch error.
	* @var TIMEOUT_ERROR
	* Timeout error.
	* @var PARM_ERROR
	* Parameter out of range error.
  *
*/
typedef enum{
  NO_ERROR       = 0x00,
  SMBUS_ERROR    = 0x01,
  PEC_ERROR 		 = 0x02,
  TIMEOUT_ERROR  = 0x04,
  PARM_ERROR     = 0x80,
}mlx90614_Status_t;

/**
	* @}
*/


/**
	* @defgroup MLX90614_UNIONS Unions
	* @brief Unions collection
	* @{
*/

/**
	* @brief The union contains PWM control register of MLX90614
*/
typedef union {
	uint16_t pwm_conf_reg;
  struct{
    uint16_t Period  				: 7; /**< PWM period. */
    uint16_t Repetition     : 5; /**< PWM Repetition. */
    uint16_t PWM_THERM_Mode : 1; /**< PWM/Thermal relay mode select. */
    uint16_t SDAPin					: 1; /**< SDA Pin configuration. */
    uint16_t Power 					: 1; /**< PWM Power. */
    uint16_t Mode   				: 1; /**< PWM mode. */
  }bit;
} mlx90614_PWM_Conf_reg_t;

/**
  * @brief The union contains configuration register 1 of MLX90614
*/
typedef union {
  uint16_t conf_reg;
  struct{
    uint16_t TestEN  			: 1; /**< Enable/Disable sensor test. */
    uint16_t Kt2Sign      : 1; /**< Sign of Kt2. */
    uint16_t Gain  				: 3; /**< Gain settings. */
    uint16_t FIR      		: 3; /**< FIR settings . */
    uint16_t KsSign      	: 1; /**< Sign of Ks. */
    uint16_t IRQuantity   : 1; /**< Sensor IR quantity. */
    uint16_t Measurement	: 2; /**< Measurement enable. */
    uint16_t RepeatTest 	: 1; /**< Repeat sensor test. */
    uint16_t IIR     			: 3; /**< IIR settings. */
  }bit;
} mlx90614_Conf_Reg_t;

/**
  * @brief The union contains flags of MLX90614
*/
typedef union {
  uint16_t flags;
  struct{
    uint16_t Zeros2  					: 8; /**< Gain settings. */
    uint16_t EEBUSY      			: 1; /**< FIR settings . */
    uint16_t Unused      			: 1; /**< Sign of Ks. */
    uint16_t EE_DEAD  				: 1; /**< Sensor IR quantity. */
    uint16_t INIT							: 1; /**< Measurement enable. */
    uint16_t NotImplemented 	: 1; /**< Repeat sensor test. */
    uint16_t Zeros1  					: 3; /**< IIR settings. */
  }bit;
} mlx90614_Flags_t;

/**
	* @}
*/

/**
	* @}
*/
float mlx90614_Get_Temp(SMBUS_HandleTypeDef *SMBUSx, uint8_t TempType);
mlx90614_Status_t mlx90614_Read_Data(SMBUS_HandleTypeDef *SMBUSx, uint8_t command, uint8_t *bufferRead, uint8_t PEC);
mlx90614_Status_t mlx90614_Write_Data(SMBUS_HandleTypeDef *SMBUSx, uint8_t command, uint16_t value_to_set);
uint8_t mlx90614_Calc_PEC(uint8_t *data,uint8_t nbrOfBytes);
mlx90614_Status_t mlx90614_Check_PEC(uint8_t* data, uint8_t nbrOfBytes, uint8_t checksum);

#endif
